#!/bin/sh

# MYSQL_USERNAME='root'
# MYSQL_PASSWORD='123456'
# MYSQL_ADDRESS='127.0.0.1'
# MYSQL_DATABASE='surveyking_app_table'

# 函数定义
create_table_if_not_exists() {
  local MYSQL_CHECK_TABLE="$1"
  local SQL_FILE="$2"

  # 执行SQL查询
  CHECK_DATABASE_SQL="SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = '$MYSQL_DATABASE';"
  DATABASE_EXISTS=$(mysql -h"$MYSQL_HOST" -P"${MYSQL_PORT-3306}" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -e"$CHECK_DATABASE_SQL" -sN)
  # 如果数据库不存在，创建数据库
  if [ "$DATABASE_EXISTS" -eq 0 ]; then
    mysqladmin -h"$MYSQL_HOST"  -P"${MYSQL_PORT-3306}" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" create "$MYSQL_DATABASE"
  fi

  # 检查表是否存在的SQL查询
  CHECK_TABLE_SQL="SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '$MYSQL_DATABASE' AND table_name = '$MYSQL_CHECK_TABLE';"
  # 执行SQL查询
  TABLE_EXISTS=$(mysql -h"$MYSQL_HOST" -P"${MYSQL_PORT-3306}" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -e"$CHECK_TABLE_SQL" -sN)
  if [ "$TABLE_EXISTS" -eq 0 ]; then
    mysql -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -h"$MYSQL_HOST" -P"${MYSQL_PORT-3306}" "$MYSQL_DATABASE" < "$SQL_FILE"
  else
    echo "表 $MYSQL_CHECK_TABLE 存在，不执行SQL文件。"
  fi
}

create_field_if_not_exists() {
  local table_name="$1"
  local field_name="$2"
  local sql_file="$3"

  # 检查字段是否存在
  exists=$(mysql -u "$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -h "$MYSQL_HOST" -D "$MYSQL_DATABASE" -e "SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_schema = '$MYSQL_DATABASE' AND TABLE_NAME='$table_name' AND COLUMN_NAME='$field_name';" -s)

  if [ "$exists" -eq 0 ]; then
    echo "字段 $field_name 不存在，将创建该字段..."
    # 执行指定的SQL文件
     mysql -u "$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -h "$MYSQL_HOST" -D "$MYSQL_DATABASE" < "$sql_file"
    echo "字段 $field_name 创建成功。"
  else
    echo "字段 $field_name 已存在，无需创建。"
  fi
}

check_field_if_not_type() {
    local table_name="$1"
    local field_name="$2"
    local expected_length="$3"
    local sql_file="$4"

    # 获取字段长度
    actual_length=$(mysql -u "$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -h "$MYSQL_HOST" -D "$MYSQL_DATABASE" -e "DESCRIBE $table_name" | awk -v field="$field_name" '$1 == field {print $2}')

    if [ "$actual_length" != "$expected_length" ]; then
        echo "字段 $field_name 修改成功。"
        mysql -u "$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -h "$MYSQL_HOST" -D "$MYSQL_DATABASE" < "$sql_file"
    else
        echo "字段 $field_name 无需修改。"
    fi
}

create_table_if_not_exists "ims_formula" "./upgrade/init-mysql.sql"
create_table_if_not_exists "ims_tag" "./upgrade/upgrade-1.0.0.sql"
create_field_if_not_exists "ims_formula" "install_total" "./upgrade/upgrade-1.0.1.sql"
create_field_if_not_exists "ims_formula" "status" "./upgrade/upgrade-1.0.2.sql"
check_field_if_not_type "ims_formula_setting" "name" "varchar(100)" "./upgrade/upgrade-1.0.3.sql"
create_field_if_not_exists "ims_version" "created_at" "./upgrade/upgrade-1.0.5.sql"

/home/server server:start -f ./config.yaml