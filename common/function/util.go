package function

import (
	"bytes"
	"crypto/md5"
	"fmt"
	w7 "github.com/w7corp/sdk-open-cloud-go"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
)

func GetRandomString(n int) string {
	str := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789"
	bytes := []byte(str)
	var result []byte
	for i := 0; i < n; i++ {
		result = append(result, bytes[rand.Intn(len(bytes))])
	}
	return string(result)
}

func GetMd5(str string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

type pathInfoOut struct {
	DirName   string
	BaseName  string
	Extension string
	Filename  string
}

func GetPathInfo(path string) *pathInfoOut {
	filename := filepath.Base(path)
	ext := filepath.Ext(filename)

	dirname, basename := filepath.Split(path)
	basename = basename[:len(basename)-len(ext)]
	result := &pathInfoOut{}
	result.DirName = dirname
	result.BaseName = basename
	result.Extension = ext
	result.Filename = filename
	return result
}

func GetSdkClient(releaseEnv bool) *w7.Client {
	apiUrl, exists := os.LookupEnv("API_URL")
	if !exists {
		apiUrl = "https://openapi.w7.cc"
	}
	var client *w7.Client
	if !releaseEnv {
		client = w7.NewClient(
			facade.GetConfig().GetString("setting.app_id"),
			facade.GetConfig().GetString("setting.app_secret"),
			w7.Option{
				ApiUrl: apiUrl,
				Debug:  false,
			})
	} else {
		client = w7.NewClient(facade.GetConfig().GetString("setting.app_id"), facade.GetConfig().GetString("setting.app_secret"))
	}
	return client
}

func CheckFileAllowUpload(filename string) bool {
	allowFileExt := []string{
		".zip", ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".cvs",
		".jpg", ".png", ".jpeg", ".gif",
	}
	for _, s := range allowFileExt {
		if strings.HasSuffix(filename, s) {
			return true
		}
	}
	return false
}

func EncodeURIComponent(s string, excluded ...[]byte) string {
	var b bytes.Buffer
	written := 0
	for i, n := 0, len(s); i < n; i++ {
		c := s[i]
		switch c {
		case '-', '_', '.', '!', '~', '*', '\'', '(', ')':
			continue
		default:
			// Unreserved according to RFC 3986 sec 2.3
			if 'a' <= c && c <= 'z' {
				continue
			}
			if 'A' <= c && c <= 'Z' {
				continue
			}
			if '0' <= c && c <= '9' {
				continue
			}
			if len(excluded) > 0 {
				conti := false
				for _, ch := range excluded[0] {
					if ch == c {
						conti = true
						break
					}
				}
				if conti {
					continue
				}
			}
		}
		b.WriteString(s[written:i])
		fmt.Fprintf(&b, "%%%02X", c)
		written = i + 1
	}
	if written == 0 {
		return s
	}
	b.WriteString(s[written:])
	return b.String()
}
