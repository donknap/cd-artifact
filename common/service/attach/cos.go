package attach

import (
	"context"
	"github.com/tencentyun/cos-go-sdk-v5"
	"github.com/tencentyun/cos-go-sdk-v5/debug"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

func NewCosClient(secretId string, secretKey string, endpoint string) *Cos {
	client := &Cos{
		secretId:  secretId,
		secretKey: secretKey,
	}
	u, _ := url.Parse(endpoint)
	client.client = cos.NewClient(&cos.BaseURL{BucketURL: u}, &http.Client{
		//设置超时时间
		Timeout: 100 * time.Second,
		Transport: &cos.AuthorizationTransport{
			SecretID:  secretId,
			SecretKey: secretKey,
			// Debug 模式，把对应 请求头部、请求内容、响应头部、响应内容 输出到标准输出
			Transport: &debug.DebugRequestTransport{
				RequestHeader:  false,
				RequestBody:    false,
				ResponseHeader: false,
				ResponseBody:   false,
			},
		},
	})
	return client
}

type Cos struct {
	client             *cos.Client
	secretId           string
	secretKey          string
	ContextHandlerFunc func() context.Context
}

func (self Cos) PresignUrlMultipart(remoteName string, partNumber int32, uploadId string) (*PresignUrl, error) {
	//TODO implement me
	panic("implement me")
}

func (self Cos) PresignUrl(remoteName string) (*PresignUrl, error) {
	println(789)
	//TODO implement me
	return &PresignUrl{}, nil
}

func (self Cos) UploadByFile(remoteName string, file *os.File) error {
	_, err := self.client.Object.Put(self.getContext(), remoteName, file, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) UploadByContent(remoteName string, content string) error {
	reader := strings.NewReader(content)
	_, err := self.client.Object.Put(self.getContext(), remoteName, reader, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) UploadByFilePath(remoteName string, localFilePath string) error {
	_, _, err := self.client.Object.Upload(self.getContext(), remoteName, localFilePath, &cos.MultiUploadOptions{
		ThreadPoolSize: 5,
	})
	//_, err := self.client.Object.PutFromFile(self.getContext(), remoteName, localFilePath, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) UploadByUploadFile(remoteName string, file multipart.File) error {
	_, err := self.client.Object.Put(self.getContext(), remoteName, file, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) GetString(remoteName string) (string, error) {
	response, err := self.client.Object.Get(self.getContext(), remoteName, nil)
	if err != nil {
		return "", err
	}
	content, _ := io.ReadAll(response.Body)
	defer response.Body.Close()
	return string(content), nil
}

func (self Cos) GetFile(remoteName string) (*os.File, error) {
	localFile, err := os.CreateTemp("", "cd-artifact")
	if err != nil {
		return nil, err
	}
	_, err = self.client.Object.GetToFile(self.getContext(), remoteName, localFile.Name(), nil)
	if err != nil {
		return nil, err
	}
	return localFile, nil
}

func (self Cos) GetSaveFile(remoteName string, localFilePath string) error {
	_, err := self.client.Object.GetToFile(self.getContext(), remoteName, localFilePath, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) Delete(remoteName string) error {
	_, err := self.client.Object.Delete(self.getContext(), remoteName, nil)
	if err != nil {
		return err
	}
	return nil
}

func (self Cos) GetUrl(remoteName string) string {
	url := self.client.Object.GetObjectURL(remoteName)
	return url.String()
}

func (self Cos) GetPrivateUrl(remoteName string, expired time.Duration) (string, error) {
	url, err := self.client.Object.GetPresignedURL(self.getContext(), http.MethodGet, remoteName, self.secretId, self.secretKey, expired, nil)
	if err != nil {
		return "", err
	}
	return url.String(), nil
}

func (self Cos) getContext() context.Context {
	if self.ContextHandlerFunc != nil {
		return self.ContextHandlerFunc()
	}
	return context.Background()
}
