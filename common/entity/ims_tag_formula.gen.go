// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.
// Code generated by gorm.io/gen. DO NOT EDIT.

package entity

const TableNameTagFormula = "ims_tag_formula"

// TagFormula mapped from table <ims_tag_formula>
type TagFormula struct {
	ID        int32    `gorm:"column:id;primaryKey;autoIncrement:true" json:"id"`
	TagID     int32    `gorm:"column:tag_id;not null" json:"tag_id"`
	FormulaID int32    `gorm:"column:formula_id;not null" json:"formula_id"`
	Formula   *Formula `gorm:"foreignKey:id;references:formula_id" json:"formula"`
	Tag       *Tag     `gorm:"foreignKey:id;references:tag_id" json:"tag"`
}

// TableName TagFormula's table name
func (*TagFormula) TableName() string {
	return TableNameTagFormula
}
