-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2023-09-21 09:12:25
-- 服务器版本： 8.0.28
-- PHP 版本： 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- 数据库： `w7-cd-artifact`
--

-- --------------------------------------------------------

--
-- 表的结构 `ims_tag`
--

CREATE TABLE `ims_tag` (
                           `id` int NOT NULL,
                           `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ims_tag_formula`
--

CREATE TABLE `ims_tag_formula` (
                                   `id` int NOT NULL,
                                   `tag_id` int NOT NULL,
                                   `formula_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `ims_tag`
--
ALTER TABLE `ims_tag`
    ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ims_tag_formula`
--
ALTER TABLE `ims_tag_formula`
    ADD PRIMARY KEY (`id`),
  ADD KEY `formula_id` (`formula_id`,`tag_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `ims_tag`
--
ALTER TABLE `ims_tag`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ims_tag_formula`
--
ALTER TABLE `ims_tag_formula`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;
