package tests

var (
	secretId   string
	secretKey  string
	url        string
	remoteName string
	endpoint   string
	bucket     string
	region     string
)
