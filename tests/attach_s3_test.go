package tests

import (
	"bytes"
	"fmt"
	"gitee.com/donknap/cd-artifact/common/service/attach"
	"github.com/stretchr/testify/assert"
	"io"
	"math"
	"net/http"
	"os"
	"testing"
	"time"
)

func TestS3(t *testing.T) {
	remoteName = "manifest.yaml"
	zipPath := "../w7_cd_artifact/manifest.yaml"
	uploadFile, _ := os.Open(zipPath)

	assert := assert.New(t)
	//client := getClient()
	client := getLocalClient()

	err := client.UploadByFile(remoteName, uploadFile)
	assert.Nil(err)

	content, err := client.GetString(remoteName)

	assert.Nil(err)
	assert.Contains(content, "application:")

	err = client.UploadByContent(remoteName, "abc")
	assert.Nil(err)

	fmt.Printf("%v \n", client.GetUrl(remoteName))
	url, err = client.GetPrivateUrl(remoteName, time.Hour)
	assert.Nil(err)
	fmt.Printf("%v \n", url)

	assert.Contains(url, "https://")

	content, err = client.GetString(remoteName)
	assert.Nil(err)
	assert.Contains(content, "abc")

	file, err := client.GetFile(remoteName)
	fmt.Printf("%v \n", file.Name())
	defer file.Close()
	defer os.Remove(file.Name())

	b, _ := io.ReadAll(file)

	assert.Contains(string(b), "abc")

	localFilePath := "/tmp/abc"
	client.GetSaveFile(remoteName, localFilePath)
	assert.FileExists(localFilePath)

	err = client.Delete(remoteName)
	content, err = client.GetString(remoteName)
	assert.NotNil(err)
	assert.Emptyf(content, "")
	client.UploadByFilePath(remoteName, "../table.yaml")
}

func TestPreSign(t *testing.T) {
	client := getClient()

	zipPath := "../w7_cd_artifact/manifest.yaml"
	uploadFile1, _ := os.Open(zipPath)
	//预签名上传
	preUrl, _ := client.PresignUrl("/test/pre1.zip")
	request, err := http.NewRequest(preUrl.Method, preUrl.Url, uploadFile1)
	fileInfo, _ := uploadFile1.Stat()
	request.ContentLength = fileInfo.Size()
	if err != nil {
		fmt.Printf("%v \n", err)
	}
	response, err := http.DefaultClient.Do(request)
	assert.Nil(t, err)
	assert.Equal(t, response.StatusCode, 200)

	zipFile := "/Users/renchao/Workspace/open-system/artifact-meedu/1.zip"
	uploadFile, _ := os.OpenFile(zipFile, os.O_RDWR, 0755)
	fileInfo, _ = uploadFile.Stat()

	totalSize := fileInfo.Size()
	partSize := int64(5 * 1024 * 1024)
	partPage := int64(math.Floor(float64(totalSize / partSize)))
	remoteName = "/test/pre2.zip"
	uploadId, err := client.MultipartCreateUploadId(remoteName)
	fmt.Printf("文件共：%v，分 %d 片，上传 id %s \n", totalSize, partPage, uploadId)
	var i int64
	for i = 1; i <= partPage; i++ {
		var part []byte
		if i == partPage {
			endSize := totalSize - (i-1)*partSize
			part = make([]byte, endSize)
			fmt.Printf("读完成：%v \n", endSize)
		} else {
			part = make([]byte, partSize)
			fmt.Printf("读取了：%v \n", partSize)
		}
		uploadFile.Read(part)
		preUrl, _ = client.PresignUrlMultipart(remoteName, uploadId, int32(i))
		request, err = http.NewRequest(preUrl.Method, preUrl.Url, bytes.NewBuffer(part))
		if err != nil {
			fmt.Printf("%v \n", err)
		}
		response, err = http.DefaultClient.Do(request)
		fmt.Printf("%v \n", response.Status)
	}
	url, err := client.MultipartComplete(uploadId)
	assert.Nil(t, err)
	assert.NotEmpty(t, url)
	fmt.Printf("%v \n", url)

}

func TestLocalSign(t *testing.T) {
	client := getLocalClient()
	url1, _ := client.GetPrivateUrl("/icon/abcd_1.icon.jpg", time.Hour)
	fmt.Printf("%v \n", url1)
}

func getClient() attach.StorageClient {
	secretId = ""
	secretKey = ""
	endpoint = ""
	bucket = "artifact"
	region = "cn-anhui-suzhou"
	remoteName = "manifest.yaml"

	client := attach.NewStorageClient(&attach.StorageInput{
		Type:      attach.STORAGE_TYPE_S3,
		SecretId:  secretId,
		SecretKey: secretKey,
		Endpoint:  endpoint,
		Bucket:    bucket,
		Region:    region,
		Debug:     false,
	})
	return client
}

func getLocalClient() attach.StorageClient {
	client := attach.NewStorageClient(&attach.StorageInput{
		Type:      attach.STORAGE_TYPE_LOCAL,
		Path:      os.TempDir() + "test/",
		SecretId:  "abc",
		SecretKey: "def",
		Endpoint:  "http://127.0.0.1:8084/zip/icon",
	})
	return client
}
