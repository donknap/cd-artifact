package attach

import (
	"gitee.com/donknap/cd-artifact/app/attach/http/controller"
	"gitee.com/donknap/cd-artifact/app/attach/http/middleware"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go-support/src/console"
	http_server "github.com/we7coreteam/w7-rangine-go/src/http/server"
)

type Provider struct {
}

func (provider *Provider) Register(httpServer *http_server.Server, console console.Console) {
	// 注册一些路由
	httpServer.RegisterRouters(func(engine *gin.Engine) {
		cors := engine.Group("/", middleware.Cors{}.Process)
		cors.Match([]string{"POST", "OPTIONS"}, "/attach/pre-upload-url", middleware.CheckUserLogin{}.Process, controller.Presign{}.PreUploadUrl)

		cors.Match([]string{"POST", "OPTIONS"}, "/attach/multipart/upload-id", middleware.CheckUserLogin{}.Process, controller.Multipart{}.UploadId)
		cors.Match([]string{"POST", "OPTIONS"}, "/attach/multipart/complete", middleware.CheckUserLogin{}.Process, controller.Multipart{}.Complete)
	})

}
