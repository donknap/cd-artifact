package controller

import (
	"errors"
	"fmt"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/function"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
	"time"
)

type Single struct {
	controller.Abstract
}

func (self Single) Upload(http *gin.Context) {
	type ParamsValidate struct {
		Filename string `form:"filename" binding:"required"`
	}
	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	if !function.CheckFileAllowUpload(params.Filename) {
		self.JsonResponseWithError(http, errors.New("不允许上传该类型文件"), 500)
		return
	}
	file, _, err := http.Request.FormFile("file")
	if err_handler.Found(err) {
		self.JsonResponseWithError(http, errors.New("请上传文件"), 500)
		return
	}
	pathInfo := function.GetPathInfo(params.Filename)
	now := time.Now().Format("2006/01/02")
	params.Filename = fmt.Sprintf("%s/%s%s", now, function.GetMd5(uuid.NewString()), pathInfo.Extension)
	logic.GetS3Client().UploadByUploadFile(params.Filename, file)
	self.JsonResponseWithoutError(http, gin.H{
		"url": params.Filename,
	})
	return
}
