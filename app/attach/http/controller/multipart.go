package controller

import (
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
)

type Multipart struct {
	controller.Abstract
}

func (self Multipart) UploadId(http *gin.Context) {
	type ParamsValidate struct {
		Filename string `form:"filename" binding:"required,endswith=.zip"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}

	client := logic.GetS3Client()
	uploadId, err := client.MultipartCreateUploadId(params.Filename)
	if err_handler.Found(err) {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	self.JsonResponseWithoutError(http, gin.H{
		"upload_id":     uploadId,
		"min_part_size": 5 * 1024 * 1024,
	})
	return
}

func (self Multipart) Complete(http *gin.Context) {
	type ParamsValidate struct {
		UploadId string `form:"upload_id" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	client := logic.GetS3Client()
	url, err := client.MultipartComplete(params.UploadId)
	if err_handler.Found(err) {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	self.JsonResponseWithoutError(http, gin.H{
		"url": url,
	})
	return
}
