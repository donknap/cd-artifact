package controller

import (
	"errors"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/function"
	"gitee.com/donknap/cd-artifact/common/service/attach"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
)

type Presign struct {
	controller.Abstract
}

func (self Presign) PreUploadUrl(ctx *gin.Context) {
	type ParamsValidate struct {
		Filename   string `form:"filename" binding:"required"`
		UploadId   string `form:"upload_id" binding:"omitempty"`
		PartNumber int32  `form:"part_number" binding:"number"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}
	if !function.CheckFileAllowUpload(params.Filename) {
		self.JsonResponseWithError(ctx, errors.New("不允许上传该类型文件"), 500)
		return
	}
	client := logic.GetS3Client()
	var url *attach.PresignUrl
	var err error
	if params.UploadId != "" && params.PartNumber > 0 {
		url, err = client.PresignUrlMultipart(params.Filename, params.UploadId, params.PartNumber)
	} else {
		url, err = client.PresignUrl(params.Filename)
	}

	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	self.JsonResponseWithoutError(ctx, gin.H{
		"url":    url.Url,
		"method": url.Method,
	})
	return
}
