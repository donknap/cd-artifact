package middleware

import (
	"errors"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/middleware"
	"strings"
)

type CheckUserLogin struct {
	middleware.Abstract
}

func (self CheckUserLogin) Process(ctx *gin.Context) {
	header := strings.Split(ctx.Request.Header.Get("Authorization"), "bearer ")
	if len(header) <= 1 {
		self.JsonResponseWithError(ctx, errors.New("请先登录"), 401)
		ctx.Abort()
		return
	}
	oauthLogic := &logic.OauthLogic{}
	userInfo, err := oauthLogic.ParseUserJwtToken(header[1])
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 401)
		ctx.Abort()
		return
	}
	ctx.Set("user", userInfo)
	ctx.Next()
}
