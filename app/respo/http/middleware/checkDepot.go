package middleware

import (
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/http/middleware"
)

type CheckDepot struct {
	middleware.Abstract
}

func (self CheckDepot) Process(ctx *gin.Context, depot *logic.Depot) {
	ctx.Set("depot", depot)
	ctx.Next()
}
