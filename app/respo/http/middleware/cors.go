package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/http/middleware"
)

type Cors struct {
	middleware.Abstract
}

func (self Cors) Process(ctx *gin.Context) {
	if host, ok := self.isAllow(ctx); ok {
		ctx.Header("Access-Control-Allow-Origin", host)
		ctx.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization")
		ctx.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		ctx.Header("Access-Control-Expose-Headers", self.getAllowHeader())
		ctx.Header("Access-Control-Allow-Credentials", "true")
		if ctx.Request.Method == "OPTIONS" {
			ctx.AbortWithStatus(http.StatusNoContent)
		}
	}
	ctx.Next()
}

func (self Cors) isAllow(ctx *gin.Context) (string, bool) {
	host := ctx.Request.Header.Get("origin")
	if host == "" {
		host = ctx.Request.Header.Get("referer")
	}
	if host == "" {
		return "", false
	}
	if true {
		return host, true
	}
	allowUrl := []string{
		"https://console.w7.cc",
		"http://console.w7.cc",
		"http://172.16.1.13:8084",
		"http://172.16.1.13",
		"http://172.16.1.13:8085",
		"http://devtool.w7.com",
	}
	for _, value := range allowUrl {
		if value == host {
			return host, true
		}
	}
	return "", false
}

func (self Cors) getAllowHeader() string {
	allowHeader := []string{
		"Content-Length",
		"Content-Type",
		"X-Auth-Token",
		"Origin",
		"Authorization",
		"X-Requested-With",
		"x-requested-with",
		"x-xsrf-token",
		"x-csrf-token",
		"x-w7-from",
		"access-token",
		"Api-Version",
		"Access-Control-Allow-Origin",
		"Access-Control-Allow-Headers",
		"Access-Control-Allow-Methods",
		"authority",
		"uid",
		"uuid",
	}
	return strings.Join(allowHeader, ",")
}
