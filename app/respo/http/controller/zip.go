package controller

import (
	"errors"
	"fmt"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/function"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
	"strconv"
	"strings"
	"time"
)

var uploadTempDir = map[string]string{}

type Zip struct {
	controller.Abstract
}

func (self Zip) Download(ctx *gin.Context) {
	type UriBind struct {
		Id string `uri:"id"`
	}
	var uriValue UriBind
	ctx.ShouldBindUri(&uriValue)

	depot, _ := logic.NewDepot()
	if formulaPath, ok := depot.DownloadMapping[uriValue.Id]; ok {
		defer delete(depot.DownloadMapping, uriValue.Id)
		if strings.HasPrefix(formulaPath, "https://") {
			ctx.Header("Location", formulaPath)
			ctx.Status(302)
			ctx.Writer.WriteHeaderNow()
		} else {
			file, err := logic.GetLocalClient().GetFile(formulaPath)
			if err != nil {
				self.JsonResponseWithError(ctx, err, 500)
				return
			}
			fileInfo, _ := file.Stat()
			ctx.Header("Content-Length", strconv.FormatInt(fileInfo.Size(), 10))
			ctx.Header("Content-Type", "application/zip")
			ctx.Header("Content-Disposition", "attachment; filename="+uriValue.Id+".zip")
			ctx.File(file.Name())
		}
	} else {
		self.JsonResponseWithError(ctx, errors.New("请先获取仓库信息"), 500)
		return
	}
}

func (self Zip) Upload(ctx *gin.Context) {
	type ParamsValidate struct {
		Filename    string `form:"filename" binding:"required,endswith=.zip|endswith=.pdf"`
		TotalChunks int32  `form:"totalChunks" binding:"required,number,gt=0"`
		Finish      int    `form:"finish" binding:"omitempty,eq=1|eq=0"`
		ChunkNumber int32  `form:"chunkNumber" binding:"required_if=Finish 0,omitempty,number,gt=0"`
		UploadId    string `form:"upload_id" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	storageLocalClient := logic.GetLocalClient()
	storageS3Client := logic.GetS3Client()

	pathInfo := function.GetPathInfo(params.Filename)
	saveFileName := fmt.Sprintf("/Storage/%s/%s%s", time.Now().Format("200601"), function.GetMd5(pathInfo.Filename), pathInfo.Extension)

	if params.Finish != 1 {
		if params.ChunkNumber < 1 || params.ChunkNumber > params.TotalChunks {
			self.JsonResponseWithError(ctx, errors.New("分卷数据错误"), 500)
			return
		}
		_, fileHeader, err := ctx.Request.FormFile("file")
		if err_handler.Found(err) {
			self.JsonResponseWithError(ctx, errors.New("请上传文件"), 500)
			return
		}

		// 此处应该按正常流程，先获取UploadId再上传
		// 为了兼容现有程序，如果没有传 UploadId 时，按 remoteName 给生成一个
		if params.UploadId == "" && uploadTempDir[params.Filename] == "" {
			uploadTempDir[params.Filename], _ = storageLocalClient.MultipartCreateUploadId(saveFileName)
		}

		uploadPartSavePath, _ := storageLocalClient.PresignUrlMultipart(params.Filename, uploadTempDir[params.Filename], params.ChunkNumber)
		ctx.SaveUploadedFile(fileHeader, uploadPartSavePath.Url)
		self.JsonSuccessResponse(ctx)
		return
	} else {
		if params.TotalChunks == 1 {
			fileUpload, fileHeader, err := ctx.Request.FormFile("file")
			if err_handler.Found(err) {
				self.JsonResponseWithError(ctx, errors.New("请上传文件"), 500)
				return
			}
			uploadSavePath, _ := logic.GetLocalClient().PresignUrl(saveFileName)
			ctx.SaveUploadedFile(fileHeader, uploadSavePath.Url)
			if storageS3Client != nil {
				err = storageS3Client.UploadByUploadFile(saveFileName, fileUpload)
				if err_handler.Found(err) {
					self.JsonResponseWithError(ctx, err, 500)
					return
				}
				logic.GetLocalClient().Delete(saveFileName)
				saveFileName = "cos://" + saveFileName
			} else {
				saveFileName = "file://" + saveFileName
			}
			self.JsonResponseWithoutError(ctx, gin.H{
				"url": saveFileName,
			})
			return
		} else {
			targetFile, err := storageLocalClient.MultipartComplete(uploadTempDir[params.Filename])
			defer delete(uploadTempDir, params.Filename)
			if err_handler.Found(err) {
				self.JsonResponseWithError(ctx, err, 500)
				return
			}

			if storageS3Client != nil {
				err := storageS3Client.UploadByFilePath(saveFileName, targetFile)
				if err_handler.Found(err) {
					self.JsonResponseWithError(ctx, err, 500)
					return
				}
				logic.GetLocalClient().Delete(saveFileName)
				saveFileName = "cos://" + saveFileName
			} else {
				saveFileName = "file://" + saveFileName
			}
		}
		self.JsonResponseWithoutError(ctx, gin.H{
			"url": saveFileName,
		})
		return
	}
}

func (self Zip) Icon(ctx *gin.Context) {
	type UriBind struct {
		Id string `uri:"id"`
	}

	remoteName := ctx.Param("path")
	if !strings.HasSuffix(remoteName, ".icon.jpg") {
		remoteName = "/icon/" + remoteName + ".icon.jpg"
	}
	if logic.GetS3Client() != nil {
		iconUrl, _ := logic.GetS3Client().GetPrivateUrl(remoteName, time.Hour*24)
		ctx.Redirect(302, iconUrl)
		ctx.Abort()
	}

	iconFile, err := logic.GetLocalClient().GetFile(remoteName)
	if err == nil {
		ctx.Header("Content-Type", "application/png")
		ctx.File(iconFile.Name())
	} else {
		ctx.String(404, "Icon not found")
		ctx.Abort()
	}
}
