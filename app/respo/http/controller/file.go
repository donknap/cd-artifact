package controller

import (
	"archive/zip"
	"errors"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
	"gopkg.in/yaml.v3"
	"io"
	"os"
	"strings"
)

type File struct {
	controller.Abstract
}

func (self File) GetZipFileList(http *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	depot, _ := logic.NewDepot()
	formula, err := depot.GetFormula(params.Identifie)
	if err != nil {
		self.JsonResponseWithError(http, errors.New("请先添加仓库"), 500)
		return
	}
	var zipFile *os.File
	if logic.GetS3Client() != nil {
		zipFile, _ = os.CreateTemp("", "artifact")
		logic.GetS3Client().GetSaveFile(formula.ZipPath, zipFile.Name())
		defer os.Remove(zipFile.Name())
	} else {
		zipFile, err = logic.GetLocalClient().GetFile(formula.ZipPath)
		if err != nil {
			self.JsonResponseWithError(http, err, 500)
			return
		}
	}
	var result []string
	zipReader, err := zip.OpenReader(zipFile.Name())
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	for _, file := range zipReader.File {
		if strings.HasPrefix(file.Name, "__MACOSX") {
			continue
		}
		result = append(result, file.Name)
	}
	self.JsonResponseWithoutError(http, gin.H{
		"list": result,
	})
	return
}

func (self File) GetZipFileContent(http *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Path      string `form:"path" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	depot, _ := logic.NewDepot()
	formula, err := depot.GetFormula(params.Identifie)
	if err != nil {
		self.JsonResponseWithError(http, errors.New("请先添加仓库"), 500)
		return
	}
	var zipFile *os.File
	if logic.GetS3Client() != nil {
		zipFile, _ = os.CreateTemp("", "artifact")
		logic.GetS3Client().GetSaveFile(formula.ZipPath, zipFile.Name())
		defer os.Remove(zipFile.Name())
	} else {
		zipFile, err = logic.GetLocalClient().GetFile(formula.ZipPath)
		if err != nil {
			self.JsonResponseWithError(http, err, 500)
			return
		}
	}
	zipReader, err := zip.OpenReader(zipFile.Name())
	file, err := zipReader.Open(params.Path)
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	content, err := io.ReadAll(file)
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	self.JsonResponseWithoutError(http, gin.H{
		"content": string(content),
	})
	return
}

func (self File) File(ctx *gin.Context) {
	var err error

	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Filename  string `form:"filename" binding:"required"`
		Content   string `form:"content" binding:"omitempty"`
		VersionId int32  `form:"version_id" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	depot, _ := logic.NewDepot()
	formula, err := depot.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	versionInfo, _ := dao.Version.Where(dao.Version.ID.Eq(params.VersionId)).First()
	if versionInfo == nil {
		self.JsonResponseWithError(ctx, errors.New("版本不存在"), 500)
		return
	}

	if params.Filename == "manifest.yaml" {
		manifest := &logic.Manifest{}
		err = yaml.Unmarshal([]byte(params.Content), manifest)
		if err_handler.Found(err) {
			self.JsonResponseWithError(ctx, errors.New("manifest 解析失败"+err.Error()), 500)
			return
		}
		if manifest.Source.Url != "" && !strings.HasSuffix(manifest.Source.Url, ".git") && !strings.HasSuffix(manifest.Source.Url, ".zip") {
			self.JsonResponseWithError(ctx, errors.New("zip_url 必须以 .git 或是 .zip 结尾"), 500)
			return
		}
		if manifest.Application.Identifie != params.Identifie {
			self.JsonResponseWithError(ctx, errors.New("manifest 中标识与仓库不一致"), 500)
			return
		}

		dao.Q.Formula.Where(dao.Formula.ID.Eq(formula.ID)).Updates(entity.Formula{
			Title: manifest.Application.Name,
		})

		if formula.Manifest.Source.Url != manifest.Source.Url {
			if strings.HasPrefix(formula.Manifest.Source.Url, "cos://") {
				remoteClient := logic.GetS3Client()
				if remoteClient != nil {
					remoteClient.Delete(formula.ZipPath)
				}
			}
			if strings.HasPrefix(formula.Manifest.Source.Url, "file://") {
				file, err := logic.GetLocalClient().GetFile(formula.ZipPath)
				if err == nil {
					os.Remove(file.Name())
				}
			}
		}
	}
	if params.Content != "" {
		err = depot.AddFile(formula, versionInfo.ID, params.Filename, params.Content, false)
	} else {
		err = depot.DeleteFile(formula, versionInfo.ID, params.Filename, false)
	}

	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}

	self.JsonSuccessResponse(ctx)
	return
}

func (self File) PathTree(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Version   string `form:"version" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	depot, _ := logic.NewDepot()
	formula, err := depot.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	var versionId int32
	versionRow, _ := dao.Q.Version.Where(dao.Q.Version.FormulaID.Eq(formula.ID),
		dao.Q.Version.Name.Eq(params.Version)).First()
	if versionRow == nil {
		if params.Version != "" {
			self.JsonResponseWithError(ctx, errors.New("版本不存在"), 500)
			return
		}
		versionId = formula.VersionId
	} else {
		versionId = versionRow.ID
	}
	fileList := formula.GetFileList(versionId)
	delete(fileList, "manifest.yaml")

	self.JsonResponse(ctx, gin.H{
		"list": fileList,
	}, nil, 200)
	return

}
