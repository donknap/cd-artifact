package controller

import (
	"errors"
	"fmt"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"gitee.com/donknap/cd-artifact/common/function"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
	"io"
	"net/http"
	"time"
)

type Home struct {
	controller.Abstract
}

func (home Home) Index(ctx *gin.Context) {
	response, _ := http.Get("https://cdn.w7.cc/web-app/w7_cd_artifact/latest/w7_cd_artifact/index.html")
	defer response.Body.Close()
	data, _ := io.ReadAll(response.Body)
	ctx.Header("Content-Type", "text/html; charset=utf-8")
	ctx.String(200, string(data))
	ctx.Abort()
}

func (home Home) Add(ctx *gin.Context) {
	var err error

	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
	}

	params := ParamsValidate{}
	if !home.Validate(ctx, &params) {
		return
	}

	depotLogin := home.getDepot()
	err = depotLogin.AddFormula(params.Identifie, "1.0.0")
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}
	home.JsonSuccessResponse(ctx)
	return
}

func (home Home) Delete(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
	}

	params := ParamsValidate{}
	if !home.Validate(ctx, &params) {
		return
	}

	depotLogin := home.getDepot()
	formula, err := depotLogin.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}

	depotLogin.DeleteFormula(formula)
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}

	home.JsonSuccessResponse(ctx)
	return
}

func (home Home) Publish(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Version   string `form:"version" binding:"omitempty,semver"`
	}

	params := ParamsValidate{}
	if !home.Validate(ctx, &params) {
		return
	}

	depotLogin := home.getDepot()
	if depotLogin.PackProcessLock.Load() {
		home.JsonResponseWithoutError(ctx, gin.H{
			"status":  logic.SYNC_STATUS_PROCESS,
			"message": "仓库正在打包中",
		})
		return
	}
	formula, err := depotLogin.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}
	if formula.ZipPath == "" && formula.Manifest.Platform.Container.Image == "" {
		home.JsonResponseWithError(ctx, errors.New("请先上传zip包或是指定镜像地址再发布"), 500)
		return
	}

	if params.Version != "" {
		versionRow := formula.GetVersionByName(params.Version)
		if versionRow == nil {
			home.JsonResponseWithError(ctx, errors.New("版本不存在"), 500)
			return
		}
		formula.VersionId = versionRow.ID
	}
	//if _, ok := formula.FileList["Dockerfile"]; !ok {
	//	home.JsonResponseWithError(ctx, errors.New("要包含 Dockerfile 文件"), 500)
	//	return
	//}

	// 如果没有zip包，只有镜像时，不需要打包发布
	// 将文件打包到 Storage 目录，需要同步的再进行同步
	if formula.ZipPath != "" || len(formula.WebZipPath) > 0 {
		depotLogin.ZipFormula(formula)
	}

	home.JsonResponseWithoutError(ctx, gin.H{
		"status":  logic.SYNC_STATUS_PROCESS,
		"message": "发起打包成功",
	})
	return
}

func (home Home) Info(ctx *gin.Context) {
	type UriBind struct {
		Id  string `uri:"id"`
		Cid string `uri:"cid"`
	}
	var uriValue UriBind
	ctx.ShouldBindUri(&uriValue)

	depotLogin := home.getDepot()
	formula, err := depotLogin.GetFormula(uriValue.Id)
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}
	if ctx.Query("from") != "" {
		dao.Q.Formula.Where(dao.Q.Formula.ID.Eq(formula.ID)).UpdateColumn(dao.Q.Formula.InstallTotal, dao.Q.Formula.InstallTotal.Add(1))
	}

	zipUrl := ""
	if formula.ZipPath != "" {
		schemaHttp := "http://"
		if ctx.Request.TLS != nil {
			schemaHttp = "https://"
		}
		str := function.GetRandomString(20)

		zipUrl = fmt.Sprintf("%s%s/zip/download/%s", schemaHttp, ctx.Request.Host, str)

		if formula.IsCosFile {
			remoteClient := logic.GetS3Client()
			if remoteClient != nil {
				depotLogin.DownloadMapping[str], _ = remoteClient.GetPrivateUrl(formula.ZipPath, time.Hour)
			} else {
				zipUrl = ""
			}
		} else {
			depotLogin.DownloadMapping[str] = formula.ZipPath
		}
	}

	var manifestContent []byte
	if uriValue.Cid != "" {
		manifestFile, _ := dao.Q.FormulaSetting.Where(
			dao.FormulaSetting.FormulaID.Eq(formula.ID),
			dao.FormulaSetting.VersionID.Eq(formula.VersionId),
			dao.FormulaSetting.Name.Eq(fmt.Sprintf("%s/manifest.yaml", uriValue.Cid)),
		).Preload(dao.Q.FormulaSetting.FormulaSettingValue).First()
		if manifestFile == nil {
			home.JsonResponseWithError(ctx, errors.New("依赖模块不存在"), 500)
			return
		}
		manifestContent = []byte(manifestFile.FormulaSettingValue.Value)
	} else {
		manifestFile, _ := dao.Q.FormulaSetting.Where(
			dao.FormulaSetting.FormulaID.Eq(formula.ID),
			dao.FormulaSetting.VersionID.Eq(formula.VersionId),
			dao.FormulaSetting.Name.Eq("manifest.yaml"),
		).Preload(dao.Q.FormulaSetting.FormulaSettingValue).First()
		if manifestFile != nil {
			manifestContent = []byte(manifestFile.FormulaSettingValue.Value)
		}
	}
	version, _ := dao.Version.Where(dao.Version.ID.Eq(formula.VersionId)).First()
	home.JsonResponseWithoutError(ctx, gin.H{
		"zip_url":  zipUrl,
		"manifest": string(manifestContent),
		"version":  version,
	})
}

func (home Home) Detail(ctx *gin.Context) {
	type UriBind struct {
		Id string `uri:"id"`
	}
	var uriValue UriBind
	ctx.ShouldBindUri(&uriValue)

	depotLogin := home.getDepot()
	formula, err := depotLogin.GetFormula(uriValue.Id)
	if err_handler.Found(err) {
		home.JsonResponseWithError(ctx, err, 500)
		return
	}
	content, _ := dao.Q.FormulaSetting.Where(dao.Q.FormulaSetting.FormulaID.Eq(formula.ID),
		dao.Q.FormulaSetting.Name.Eq("readme.md"),
		dao.Q.FormulaSetting.VersionID.Eq(formula.VersionId)).Preload(dao.Q.FormulaSetting.FormulaSettingValue).First()
	var readme string
	if content == nil {
		readme = "请添加 readme.md 文件"
	} else {
		readme = content.FormulaSettingValue.Value
	}

	var versionList []map[string]interface{}
	dao.Version.
		Where(dao.Version.FormulaID.Eq(formula.ID)).
		Limit(10).Select(dao.Version.Name, dao.Version.ID, dao.Version.Description).
		Order(dao.Version.ID.Desc()).
		Scan(&versionList)

	home.JsonResponseWithoutError(ctx, gin.H{
		"title":        formula.Manifest.Application.Name,
		"description":  formula.Manifest.Application.Description,
		"icon":         formula.Icon,
		"content":      readme,
		"version_list": versionList,
	})
	return
}

func (self Home) List(ctx *gin.Context) {
	depotLogin := self.getDepot()
	type ParamsValidate struct {
		Limit   int     `form:"limit,default=30" binding:"omitempty"`
		Page    int     `form:"page,default=1" binding:"omitempty,gt=0"`
		Tag     string  `form:"tag" binding:"omitempty"`
		Keyword string  `form:"keyword" binding:"omitempty"`
		Sort    string  `form:"sort,default=new" binding:"omitempty,oneof=hot new"`
		Status  []int32 `form:"status"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	if params.Page < 1 {
		params.Page = 1
	}

	if len(params.Status) == 0 {
		params.Status = append(params.Status, 2)
		params.Status = append(params.Status, 99)
	}

	type ResultNode struct {
		Name         string          `json:"name"`
		Description  string          `json:"description"`
		Identifie    string          `json:"identifie"`
		Icon         string          `json:"icon"`
		Tag          []entity.Tag    `json:"tag"`
		InstallTotal int32           `json:"install_total"`
		Version      *entity.Version `json:"version"`
	}

	var result []ResultNode

	query := dao.Q.Formula.Preload(dao.Formula.Tag, dao.Formula.Version)
	query = query.Where(dao.Q.Formula.Status.In(params.Status...))

	if params.Tag != "" {
		searchTag, _ := dao.Q.Tag.Where(dao.Q.Tag.Name.Eq(params.Tag)).First()
		if searchTag == nil {
			self.JsonResponseWithError(ctx, errors.New("标签不存在"), 500)
			return
		}
		formulaTagList, _, _ := dao.Q.TagFormula.Where(dao.Q.TagFormula.TagID.Eq(searchTag.ID)).
			FindByPage((params.Page-1)*params.Limit, params.Limit)
		if formulaTagList != nil {
			var searchFormulaIds []int32
			for _, tagItem := range formulaTagList {
				searchFormulaIds = append(searchFormulaIds, tagItem.FormulaID)
			}
			query = query.Where(dao.Q.Formula.ID.In(searchFormulaIds...))
		}
	}

	if params.Keyword != "" {
		query = query.Where(dao.Q.Formula.Title.Like("%" + params.Keyword + "%"))
	}

	if params.Sort == "new" || params.Sort == "" {
		query = query.Order(dao.Formula.Status.Desc(), dao.Formula.ID.Desc())
	}
	if params.Sort == "hot" {
		query = query.Order(dao.Formula.Status.Desc(), dao.Formula.InstallTotal.Desc())
	}

	formulaList, total, _ := query.FindByPage((params.Page-1)*params.Limit, params.Limit)
	if formulaList != nil {
		for _, item := range formulaList {
			formula, err := depotLogin.GetFormula(item.Name)
			if err == nil {
				resultItem := ResultNode{
					Name:         item.Title,
					Description:  formula.Manifest.Application.Description,
					Identifie:    item.Name,
					Icon:         formula.Icon,
					Tag:          item.Tag,
					InstallTotal: item.InstallTotal,
					Version:      item.Version,
				}
				result = append(result, resultItem)
			}
		}
	}

	self.JsonResponseWithoutError(ctx, gin.H{
		"total": total,
		"limit": params.Limit,
		"page":  params.Page,
		"list":  result,
	})
	return
}

func (self Home) Icon(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}
	depotLogin := self.getDepot()
	formula, err := depotLogin.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, errors.New("请先添加仓库"), 500)
		return
	}

	uploadFile, fileHeader, err := ctx.Request.FormFile("file")
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, errors.New("请上传文件"), 500)
		return
	}
	if fileHeader.Size > 1024*1024 {
		self.JsonResponseWithError(ctx, errors.New("图标大小不能超过1m"), 500)
		return
	}
	remoteName := "/icon/" + formula.Name + ".icon.jpg"
	content, _ := io.ReadAll(uploadFile)
	remoteClient := logic.GetStorageClient()

	err = remoteClient.UploadByContent(remoteName, string(content))
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	err = depotLogin.AddFile(formula, formula.VersionId, "icon.jpg", remoteName, false)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	url, err := remoteClient.GetPrivateUrl(remoteName, time.Hour*24)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}

	self.JsonResponseWithoutError(ctx, gin.H{
		"url": url,
	})
	return
}

func (self Home) Status(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Status    int    `form:"status" binding:"required,oneof=1 2 99"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}
	depotLogin := self.getDepot()
	formula, err := depotLogin.GetFormula(params.Identifie)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, errors.New("请先添加仓库"), 500)
		return
	}
	dao.Formula.Where(dao.Formula.ID.Eq(formula.ID)).Update(dao.Formula.Status, params.Status)
	self.JsonSuccessResponse(ctx)
	return
}

func (self Home) getDepot() *logic.Depot {
	depot, _ := logic.NewDepot()
	return depot
}
