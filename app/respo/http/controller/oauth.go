package controller

import (
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/function"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
)

type Oauth struct {
	controller.Abstract
}

func (self Oauth) Login(ctx *gin.Context) {
	type ParamsValidate struct {
		Code string `form:"code" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	client := function.GetSdkClient(false)
	accessToken, err := client.OauthService.GetAccessTokenByCode(params.Code)
	if err_handler.Found(err.ToError()) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}
	userInfo, err := client.OauthService.GetUserInfo(accessToken.AccessToken)
	if err_handler.Found(err.ToError()) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}

	token := &logic.LoginUserToken{
		Nickname: userInfo.Nickname,
		OpenId:   userInfo.OpenId,
	}
	oauthLogic := &logic.OauthLogic{}
	self.JsonResponseWithoutError(ctx, gin.H{
		"code": oauthLogic.GetUserJwtToken(token),
	})
	return
}
