package controller

import (
	"errors"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
	"gorm.io/gen"
)

type Version struct {
	controller.Abstract
}

func (self Version) Add(http *gin.Context) {
	type ParamsValidate struct {
		Identifie   string `form:"identifie" binding:"required"`
		Version     string `form:"version" binding:"required,semver"`
		Description string `form:"description" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}

	formula, err := dao.Formula.Where(dao.Formula.Name.Eq(params.Identifie)).First()
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	var versionRow *entity.Version
	where := []gen.Condition{
		dao.Q.Version.FormulaID.Eq(formula.ID),
		dao.Q.Version.Name.Eq(params.Version),
	}
	versionRow, err = dao.Q.Version.Where(where...).First()
	if versionRow != nil {
		_, err = dao.Q.Version.Where(dao.Q.Version.ID.Eq(versionRow.ID)).Update(dao.Q.Version.Description, params.Description)
	} else {
		versionRow = &entity.Version{
			Name:        params.Version,
			Description: params.Description,
			FormulaID:   formula.ID,
		}
		err = dao.Q.Version.Create(versionRow)
		// 复制旧版本的配置
		formulaSetting, _ := dao.FormulaSetting.Where(
			dao.FormulaSetting.FormulaID.Eq(formula.ID),
			dao.FormulaSetting.VersionID.Eq(formula.VersionLatestID)).Find()
		for _, item := range formulaSetting {
			data := item
			data.VersionID = versionRow.ID
			data.ID = 0
			dao.Q.FormulaSetting.Create(data)
		}
	}
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	self.JsonResponseWithoutError(http, gin.H{
		"id":      versionRow.ID,
		"version": versionRow.Name,
	})
	return
}

func (self Version) GetList(http *gin.Context) {
	type ParamsValidate struct {
		Limit     int    `form:"limit,default=30" binding:"omitempty"`
		Page      int    `form:"page,default=1" binding:"omitempty,gt=0"`
		Identifie string `form:"identifie" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	if params.Page < 1 {
		params.Page = 1
	}
	if params.Limit < 1 {
		params.Limit = 10
	}
	formula, err := dao.Formula.Where(dao.Formula.Name.Eq(params.Identifie)).First()
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}

	result, total, _ := dao.Version.Where(dao.Version.FormulaID.Eq(formula.ID)).
		Order(dao.Version.ID.Desc()).
		FindByPage((params.Page-1)*params.Limit, params.Limit)

	self.JsonResponseWithoutError(http, gin.H{
		"total": total,
		"limit": params.Limit,
		"page":  params.Page,
		"list":  result,
	})
	return

}

func (self Version) Publish(http *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		VersionId int32  `form:"version_id" binding:"required"`
	}
	params := ParamsValidate{}
	if !self.Validate(http, &params) {
		return
	}
	formula, err := dao.Formula.Where(dao.Formula.Name.Eq(params.Identifie)).First()
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	version, _ := dao.Version.Where(dao.Version.ID.Eq(params.VersionId)).First()
	if version == nil {
		self.JsonResponseWithError(http, errors.New("版本不存在"), 500)
		return
	}
	if version.FormulaID != formula.ID {
		self.JsonResponseWithError(http, errors.New("当前版本不属于该制品库"), 500)
		return
	}
	_, err = dao.Formula.Where(dao.Formula.ID.Eq(formula.ID)).Update(dao.Formula.VersionLatestID, params.VersionId)
	if err != nil {
		self.JsonResponseWithError(http, err, 500)
		return
	}
	depot, _ := logic.NewDepot()
	depot.ClearFormulaCache(formula.ID)
	self.JsonSuccessResponse(http)
	return
}
