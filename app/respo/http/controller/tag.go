package controller

import (
	"errors"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
)

type Tag struct {
	controller.Abstract
}

func (self Tag) Add(ctx *gin.Context) {
	type ParamsValidate struct {
		Identifie string `form:"identifie" binding:"required"`
		Name      string `form:"name" binding:"required"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	formula, err := logic.GetFormulaByName(params.Identifie)
	if err_handler.Found(err) {
		self.JsonResponseWithError(ctx, err, 500)
		return
	}

	tag, _ := dao.Q.Tag.Where(dao.Q.Tag.Name.Eq(params.Name)).First()
	if tag == nil {
		tag = &entity.Tag{
			Name: params.Name,
		}
		dao.Q.Tag.Create(tag)
	}

	formulaTag, _ := dao.TagFormula.Where(dao.TagFormula.Where(dao.TagFormula.TagID.Eq(tag.ID),
		dao.TagFormula.FormulaID.Eq(formula.ID)),
	).First()

	if formulaTag == nil {
		dao.TagFormula.Create(&entity.TagFormula{
			FormulaID: formula.ID,
			TagID:     tag.ID,
		})
	}

	self.JsonSuccessResponse(ctx)
	return
}

func (self Tag) Delete(ctx *gin.Context) {
	type ParamsValidate struct {
		TagId     int `form:"tag_id" binding:"required"`
		FormulaId int `form:"formula_id" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	tag, _ := dao.Tag.Where(dao.Tag.ID.Eq(int32(params.TagId))).First()
	if tag == nil {
		self.JsonResponseWithError(ctx, errors.New("标签不存在"), 500)
		return
	}

	if params.FormulaId == 0 {
		dao.Q.Transaction(func(tx *dao.Query) error {
			tx.Tag.Where(tx.Tag.ID.Eq(tag.ID)).Delete()
			tx.TagFormula.Where(tx.TagFormula.TagID.Eq(tag.ID)).Delete()
			return nil
		})
	} else {
		dao.Q.Transaction(func(tx *dao.Query) error {
			tx.TagFormula.Where(tx.TagFormula.TagID.Eq(tag.ID), tx.TagFormula.FormulaID.Eq(int32(params.FormulaId))).Delete()
			return nil
		})
	}
	self.JsonSuccessResponse(ctx)
	return
}

func (self Tag) List(ctx *gin.Context) {
	type ParamsValidate struct {
		Limit int `form:"limit" binding:"omitempty"`
	}

	params := ParamsValidate{}
	if !self.Validate(ctx, &params) {
		return
	}

	tag, _ := dao.Tag.Limit(params.Limit).Find()
	self.JsonResponseWithoutError(ctx, gin.H{
		"list": tag,
	})
	return
}
