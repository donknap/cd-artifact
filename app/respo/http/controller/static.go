package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
	"github.com/we7coreteam/w7-rangine-go/src/http/controller"
)

type Static struct {
	controller.Abstract
}

func (self Static) File(ctx *gin.Context) {
	local := facade.GetConfig().GetString("setting.storage.local.path") + "/Static"
	path := ctx.Param("path")
	ctx.File(local + "/" + path)
}
