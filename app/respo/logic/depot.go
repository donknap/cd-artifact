package logic

import (
	"archive/zip"
	"fmt"
	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"gitee.com/donknap/cd-artifact/common/service"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"gopkg.in/yaml.v3"
	"gorm.io/gen"
	"io"
	"log"
	"log/slog"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync/atomic"
	"syscall"
)

const (
	SYNC_STATUS_PROCESS = 1
	SYNC_STATUS_FINISH  = 2
	SYNC_STATUS_FAILED  = 3
)

const (
	SETTING_TYPE_FILE    = 1
	SETTING_TYPE_SETTING = 2
	SETTING_TYPE_ICON    = 3
)

func RegisterDepot() error {
	err := facade.GetContainer().NamedSingleton("depot", func() *Depot {
		depot := &Depot{}
		depot.basePath = strings.TrimRight(facade.GetConfig().GetString("setting.storage.local.path"), "/")
		depot.packChan = make(chan *Formula)
		depot.DownloadMapping = make(map[string]string)
		depot.formulaMapping = make(map[int32]*Formula)
		return depot
	})
	return err
}

func NewDepot() (*Depot, error) {
	var obj *Depot
	err := facade.GetContainer().NamedResolve(&obj, "depot")
	if err != nil {
		return nil, err
	}
	return obj, nil
}

type Depot struct {
	basePath        string
	formulaMapping  map[int32]*Formula // 已添加仓库结构
	DownloadMapping map[string]string
	packChan        chan *Formula
	PackProcessLock atomic.Bool
}

func (self *Depot) AddFormula(name string, version string) error {
	formulaPath := fmt.Sprintf("%s/Formula/%s", self.basePath, name)
	_, err := os.Stat(formulaPath)
	if os.IsNotExist(err) {
		_ = os.MkdirAll(formulaPath, service.FileMode)
	}
	dao.Q.Transaction(func(tx *dao.Query) error {
		row, _ := tx.Formula.Where(tx.Formula.Name.Eq(name)).First()
		if row == nil {
			versionData := &entity.Version{
				Name:        version,
				Description: "",
			}
			dao.Q.Version.Create(versionData)
			formulaData := &entity.Formula{
				Name:            name,
				VersionLatestID: versionData.ID,
			}
			dao.Q.Formula.Create(formulaData)
			dao.Q.Version.Where(dao.Q.Version.ID.Eq(versionData.ID)).Update(dao.Version.FormulaID, formulaData.ID)
		}
		return nil
	})

	return nil
}

func (self *Depot) GetFormula(id string) (*Formula, error) {
	row, _ := dao.Q.Formula.Where(dao.Formula.Name.Value(id)).First()
	if row == nil {
		return nil, err_handler.Throw("仓库不存在请先添加", nil)
	}

	var result *Formula
	if item, ok := self.formulaMapping[row.ID]; ok {
		return item, nil
	}

	result = &Formula{
		ID:         row.ID,
		Name:       row.Name,
		VersionId:  row.VersionLatestID,
		Manifest:   &Manifest{},
		RootPath:   fmt.Sprintf("%s/Formula/%s", self.basePath, row.Name),
		ZipPath:    "",
		WebZipPath: map[string]string{},
		IsCosFile:  false,
		Icon:       "/zip/icon/" + row.Name,
	}

	err := result.loadManifest()
	if err_handler.Found(err) {
		return nil, err
	}

	err = result.loadAllManifest()
	if err != nil {
		return nil, err
	}

	if strings.HasPrefix(result.Manifest.Source.Url, "file://") {
		zipPath := strings.Split(result.Manifest.Source.Url, "file://")[1]
		if os.IsNotExist(err) {
			_, err := os.Stat(self.basePath + zipPath)
			slog.Debug("zip path:", "path", self.basePath+zipPath, err.Error())
			result.ZipPath = ""
		} else {
			result.ZipPath = zipPath
		}
		result.IsCosFile = false
	}

	if strings.HasPrefix(result.Manifest.Source.Url, "cos://") {
		zipPath := strings.Split(result.Manifest.Source.Url, "cos://")[1]
		result.ZipPath = zipPath
		result.IsCosFile = true
	}

	for _, webManifest := range result.AllManifest {
		if strings.HasPrefix(webManifest.Web.Url, "file://") {
			zipPath := strings.Split(webManifest.Web.Url, "file://")[1]
			if os.IsNotExist(err) {
				_, err := os.Stat(self.basePath + zipPath)
				slog.Debug("zip path:", "path", self.basePath+zipPath, err.Error())
				result.WebZipPath[webManifest.Application.Identifie] = ""
			} else {
				result.WebZipPath[webManifest.Application.Identifie] = zipPath
			}
		}

		if strings.HasPrefix(webManifest.Web.Url, "cos://") {
			zipPath := strings.Split(webManifest.Web.Url, "cos://")[1]
			result.WebZipPath[webManifest.Application.Identifie] = zipPath
			result.IsCosFile = true
		}
	}

	self.formulaMapping[row.ID] = result
	return result, nil
}

func (self *Depot) AddFile(formula *Formula, versionId int32, filename string, content string, saveFile bool) error {
	where := []gen.Condition{
		dao.FormulaSetting.FormulaID.Eq(formula.ID),
		dao.FormulaSetting.Name.Eq(filename),
		dao.FormulaSetting.VersionID.Eq(versionId),
	}
	row, _ := dao.Q.FormulaSetting.Preload(dao.Q.FormulaSetting.FormulaSettingValue).Where(where...).First()
	if row == nil {
		dao.Q.Transaction(func(tx *dao.Query) error {
			settingValueRow := &entity.FormulaSettingValue{
				Value: content,
			}
			tx.FormulaSettingValue.Create(settingValueRow)

			data := &entity.FormulaSetting{
				FormulaID:      formula.ID,
				Name:           filename,
				Type:           SETTING_TYPE_FILE,
				VersionID:      versionId,
				SettingValueID: settingValueRow.ID,
			}
			if filename == "icon.jpg" {
				data.Type = SETTING_TYPE_ICON
			}
			tx.FormulaSetting.Create(data)
			return nil
		})

	} else {
		dao.Q.Transaction(func(tx *dao.Query) error {
			// 如果是复制配置，更改数据后需要新建一条
			copySetting, _ := tx.FormulaSetting.Where(tx.FormulaSetting.SettingValueID.Eq(row.FormulaSettingValue.ID)).First()

			if row.FormulaSettingValue.ID > 0 && (copySetting == nil || copySetting.VersionID == row.VersionID) {
				tx.FormulaSettingValue.Where(tx.FormulaSettingValue.ID.Eq(row.FormulaSettingValue.ID)).Update(tx.FormulaSettingValue.Value, content)
			} else {
				settingValueRow := &entity.FormulaSettingValue{
					Value: content,
				}
				tx.FormulaSettingValue.Create(settingValueRow)
				tx.FormulaSetting.Where(tx.FormulaSetting.ID.Eq(row.ID)).Update(tx.FormulaSetting.SettingValueID, settingValueRow.ID)
			}
			return nil
		})
	}
	if saveFile {
		self.SaveFile(formula.Name, versionId, filename, content)
	}
	self.ClearFormulaCache(formula.ID)
	return nil
}

func (self *Depot) SaveFile(formulaName string, version int32, filename string, content string) error {
	filePath := fmt.Sprintf("%s/Formula/%s/%d/%s", self.basePath, formulaName, version, filename)
	os.MkdirAll(filepath.Dir(filePath), service.FileMode)
	file, _ := os.Create(filePath)
	defer file.Close()
	file.Write([]byte(content))
	return nil
}

func (self *Depot) DeleteFile(formula *Formula, versionId int32, filename string, deleteFile bool) error {
	dao.Q.Transaction(func(tx *dao.Query) error {
		where := []gen.Condition{
			dao.FormulaSetting.FormulaID.Eq(formula.ID),
			dao.FormulaSetting.Name.Eq(filename),
			dao.FormulaSetting.VersionID.Eq(versionId),
		}
		row, _ := dao.Q.FormulaSetting.Where(where...).First()
		if row != nil {
			tx.FormulaSettingValue.Where(tx.FormulaSettingValue.ID.Eq(row.SettingValueID)).Delete()
			tx.FormulaSetting.Where(tx.FormulaSetting.ID.Eq(row.ID)).Delete()
		}
		return nil
	})
	if deleteFile {
		os.Remove(fmt.Sprintf("%s/Formula/%s/%d/%s", self.basePath, formula.Name, versionId, filename))
	}
	self.ClearFormulaCache(formula.ID)
	return nil
}

func (self *Depot) DeleteFormula(formula *Formula) error {
	dao.Q.Transaction(func(tx *dao.Query) error {
		var settingValueId []int32
		query := tx.FormulaSetting.Where(tx.FormulaSetting.FormulaID.Eq(formula.ID))
		query.Pluck(tx.FormulaSetting.SettingValueID, &settingValueId)
		tx.FormulaSettingValue.Where(tx.FormulaSettingValue.ID.In(settingValueId...)).Delete()

		tx.Formula.Where(tx.Formula.ID.Eq(formula.ID)).Delete()
		query.Delete()

		self.ClearFormulaCache(formula.ID)

		formulaPath := fmt.Sprintf("%s/Formula/%s", self.basePath, formula.Name)
		os.RemoveAll(formulaPath)
		return nil
	})
	return nil
}

func (self *Depot) ClearFormulaCache(id int32) {
	delete(self.formulaMapping, id)
}

func (self *Depot) ZipFormula(formula *Formula) error {
	self.packChan <- formula
	return nil
}

func (self *Depot) InitBaseDepot() error {
	mask := syscall.Umask(0)
	defer syscall.Umask(mask)
	// 如果没有，则建立本地目录
	_, err := os.Stat(self.basePath)
	if os.IsNotExist(err) {
		err = os.MkdirAll(self.basePath, service.FileMode)
		if err_handler.Found(err) {
			return err_handler.Throw("资源仓库创建失败，请检查配置目录权限", err)
		}
	}
	_, err = os.Stat(fmt.Sprintf("%s/Formula", self.basePath))
	if os.IsNotExist(err) {
		os.MkdirAll(fmt.Sprintf("%s/Formula", self.basePath), service.FileMode)
	}
	_, err = os.Stat(fmt.Sprintf("%s/Storage", self.basePath))
	if os.IsNotExist(err) {
		os.MkdirAll(fmt.Sprintf("%s/Storage", self.basePath), service.FileMode)
	}
	return err
}

func (self *Depot) GetBaseDepotPath() string {
	return strings.TrimRight(facade.GetConfig().GetString("setting.storage.local.path"), "/")
}

func (self *Depot) SyncZip() {
	for true {
		select {
		case formula := <-self.packChan:
			log.Println("开始打包项目: ", formula.Name)
			self.PackProcessLock.Swap(true)
			if strings.HasSuffix(formula.Manifest.Source.Url, ".git") {
				self.makeFromGit(formula)
			}
			if strings.HasSuffix(formula.Manifest.Source.Url, ".zip") || strings.HasSuffix(formula.Manifest.Web.Url, ".zip") {
				self.makeFromZip(formula)
			}
			self.ClearFormulaCache(formula.ID)
			self.PackProcessLock.Swap(false)
			log.Println("打包完成")
		}
	}
}

func (self *Depot) makeFromGit(formula *Formula) {
	gitLogic := git{
		depot: self,
	}
	gitTempPath, _ := os.MkdirTemp(fmt.Sprintf("%s/Storage", self.basePath), formula.Name)
	gitLogic.runCmd("clone", formula.Manifest.Source.Url, gitTempPath)

	log.Println("打包 Manifest.yaml")
	file, _ := os.Create(gitTempPath + "/manifest.yaml")
	manifestContent, _ := yaml.Marshal(formula.Manifest)
	file.Write(manifestContent)
	file.Close()

	fileList := formula.GetFileList(formula.VersionId)
	if len(fileList) > 0 {
		for key, value := range fileList {
			file, _ = os.Create(gitTempPath + "/" + key)
			file.Write([]byte(value))
			file.Close()
		}
	}
	log.Println("打包 zip ")

	cmd := exec.Command("zip", "-r", fmt.Sprintf("../%s.zip", formula.Name), ".")
	cmd.Dir = gitTempPath
	_, err := cmd.CombinedOutput()
	cmd.Run()
	if err_handler.Found(err) {
		log.Println(err.Error())
	}
	os.RemoveAll(gitTempPath)

	formula.ZipPath = "/Storage/" + formula.Name + ".zip"
	formula.IsCosFile = false

	if GetS3Client() != nil {
		log.Println("正在上传 Cos")
		formula.IsCosFile = true
		err = GetS3Client().UploadByFilePath(formula.ZipPath, self.basePath+formula.ZipPath)
		if err != nil {
			log.Println(err.Error())
		}
		os.Remove(self.basePath + formula.ZipPath)
	}
}

func (self Depot) makeFromZip(formula *Formula) {
	var zipPath string
	var webUnzipPath string

	zipPath = self.basePath + formula.ZipPath

	if formula.ZipPath != "" {
		if formula.IsCosFile {
			if GetS3Client() != nil {
				GetS3Client().GetSaveFile(formula.ZipPath, zipPath)
				defer GetS3Client().UploadByFilePath(formula.ZipPath, zipPath)
				defer os.Remove(zipPath)
			} else {
				log.Println("远程配置丢失")
				return
			}
		}
		fileList := formula.GetFileList(formula.VersionId)

		if fileList != nil && len(fileList) > 0 {
			log.Println("开始打包配置文件")
			for name, value := range fileList {
				if value != "" {
					tempPath := self.basePath + "/Storage/" + formula.Name + "/" + name
					os.MkdirAll(filepath.Dir(tempPath), service.FileMode)
					file, err := os.Create(tempPath)
					if err != nil {
						log.Println(err.Error())
						return
					}
					file.WriteString(value)
					file.Close()
					cmd := exec.Command("zip", "-u", zipPath, name)
					log.Println("zip -u " + zipPath + " " + name)

					cmd.Dir = self.basePath + "/Storage/" + formula.Name
					message, _ := cmd.CombinedOutput()
					cmd.Run()
					println(string(message))
				}
			}
			os.RemoveAll(self.basePath + "/Storage/" + formula.Name)
		}
	}
	// 放前端包
	for name, webZipPath := range formula.WebZipPath {
		webUnzipPath = fmt.Sprintf("%s/Static/%s", self.basePath, name)

		if webZipPath != "" {
			log.Println("开始解压前端包" + name)
			if formula.IsCosFile {
				if GetS3Client() != nil {
					GetS3Client().GetSaveFile(webZipPath, self.basePath+webZipPath)
					defer os.Remove(self.basePath + webZipPath)
				} else {
					log.Println("远程配置丢失")
					return
				}
			}
			reader, err := zip.OpenReader(self.basePath + webZipPath)
			if err != nil {
				log.Println(err.Error())
				return
			}
			defer reader.Close()
			for _, file := range reader.File {
				if !file.FileInfo().IsDir() {
					log.Println("解压" + webUnzipPath + "/" + file.Name)
					_ = os.MkdirAll(filepath.Dir(webUnzipPath+"/"+file.Name), service.FileMode)
					targetFile, err := os.OpenFile(webUnzipPath+"/"+file.Name, os.O_CREATE|os.O_RDWR|os.O_TRUNC, service.FileMode)
					if err != nil {
						log.Println(err.Error())
						continue
					}
					sourceFile, _ := file.Open()
					io.Copy(targetFile, sourceFile)
					targetFile.Close()
					sourceFile.Close()
				}
			}
		}
	}
}
