package logic

import (
	"gitee.com/donknap/cd-artifact/common/service/attach"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
)

func GetS3Client() attach.StorageClient {
	client := attach.NewStorageClient(&attach.StorageInput{
		Type:      attach.STORAGE_TYPE_S3,
		SecretId:  facade.GetConfig().GetString("setting.storage.s3.secret_id"),
		SecretKey: facade.GetConfig().GetString("setting.storage.s3.secret_key"),
		Endpoint:  facade.GetConfig().GetString("setting.storage.s3.endpoint"),
		Bucket:    facade.GetConfig().GetString("setting.storage.s3.bucket"),
		Region:    facade.GetConfig().GetString("setting.storage.s3.region"),
		Debug:     true,
	})
	return client
}

func GetLocalClient() attach.StorageClient {
	client := attach.NewStorageClient(&attach.StorageInput{
		Type:      attach.STORAGE_TYPE_LOCAL,
		Path:      facade.GetConfig().GetString("setting.storage.local.path"),
		Endpoint:  facade.GetConfig().GetString("setting.storage.local.endpoint"),
		SecretId:  facade.GetConfig().GetString("setting.storage.s3.secret_id"),
		SecretKey: facade.GetConfig().GetString("setting.storage.s3.secret_key"),
	})
	return client
}

func GetStorageClient() attach.StorageClient {
	if facade.GetConfig().GetString("setting.storage.s3.secret_id") != "" {
		return GetS3Client()
	} else {
		return GetLocalClient()
	}
}
