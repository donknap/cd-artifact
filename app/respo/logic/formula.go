package logic

import (
	"errors"
	"strings"
	"time"

	"gitee.com/donknap/cd-artifact/common/dao"
	"gitee.com/donknap/cd-artifact/common/entity"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"gopkg.in/yaml.v3"
	"gorm.io/gen"
	"gorm.io/gorm"
)

const (
	FORMULA_HIDE      = 1
	FORMULA_DISPLAY   = 2
	FORMULA_RECOMMEND = 99
)

func GetFormulaByName(formulaName string) (*entity.Formula, error) {
	row, _ := dao.Q.Formula.Where(dao.Formula.Name.Eq(formulaName)).First()
	if row == nil {
		return nil, errors.New("制品库不存在")
	}
	return row, nil
}

type Formula struct {
	ID          int32
	Name        string
	VersionId   int32
	Icon        string
	Manifest    *Manifest
	AllManifest []*Manifest
	ZipPath     string
	WebZipPath  map[string]string
	CosPath     string
	IsCosFile   bool
	RootPath    string
}

func (self *Formula) GetFileList(versionId int32) map[string]string {
	result := make(map[string]string)
	settingList, _ := dao.Q.FormulaSetting.Where(
		dao.FormulaSetting.FormulaID.Eq(self.ID),
		dao.FormulaSetting.VersionID.Eq(versionId),
		dao.FormulaSetting.Type.Eq(SETTING_TYPE_FILE),
	).Preload(dao.Q.FormulaSetting.FormulaSettingValue).Find()

	if len(settingList) > 0 {
		for _, setting := range settingList {
			if setting.FormulaSettingValue != nil {
				result[setting.Name] = setting.FormulaSettingValue.Value
			}
		}
	}
	return result
}

func (self *Formula) GetVersionByName(version string) *entity.Version {
	versionRow, _ := dao.Q.Version.Where(
		dao.Version.FormulaID.Eq(self.ID),
		dao.Version.Name.Eq(version),
	).First()
	return versionRow
}

func (self *Formula) loadManifest() error {
	manifest, _ := dao.Q.FormulaSetting.Where([]gen.Condition{
		dao.Q.FormulaSetting.FormulaID.Eq(self.ID),
		dao.Q.FormulaSetting.Name.Eq("manifest.yaml"),
		dao.Q.FormulaSetting.VersionID.Eq(self.VersionId),
	}...).Preload(dao.Q.FormulaSetting.FormulaSettingValue).First()

	if manifest != nil {
		err := yaml.Unmarshal([]byte(manifest.FormulaSettingValue.Value), self.Manifest)
		if err_handler.Found(err) {
			return err
		}
	}
	return nil
}

func (self *Formula) loadAllManifest() error {
	manifest, _ := dao.Q.FormulaSetting.Where([]gen.Condition{
		dao.Q.FormulaSetting.FormulaID.Eq(self.ID),
		dao.Q.FormulaSetting.Name.Like("%manifest.yaml"),
		dao.Q.FormulaSetting.VersionID.Eq(self.VersionId),
	}...).Preload(dao.Q.FormulaSetting.FormulaSettingValue).Find()

	for _, row := range manifest {
		manifestRow := &Manifest{}
		if row.FormulaSettingValue == nil {
			continue
		}
		err := yaml.Unmarshal([]byte(row.FormulaSettingValue.Value), manifestRow)
		if err_handler.Found(err) {
			return err
		}
		self.AllManifest = append(self.AllManifest, manifestRow)
	}
	return nil
}

func (self *Formula) loadIcon() {
	if self.Icon != "" {
		if strings.HasPrefix(self.Icon, "https://") {
			//  如果有 icon 并且在远程附件，则刷新链接时间
			self.Icon, _ = GetS3Client().GetPrivateUrl("icon/"+self.Name+".icon.jpg", time.Hour*24)
		}

		return
	}

	iconSetting, err := dao.Q.FormulaSetting.Where(
		dao.FormulaSetting.FormulaID.Eq(self.ID),
		dao.FormulaSetting.Name.Eq("icon.jpg"),
	).Preload(dao.Q.FormulaSetting.FormulaSettingValue).First()

	self.Icon = ""
	if !errors.Is(err, gorm.ErrRecordNotFound) {
		if strings.HasPrefix(iconSetting.FormulaSettingValue.Value, "/icon/") {
			if GetS3Client() != nil {
				self.Icon, _ = GetS3Client().GetPrivateUrl(iconSetting.FormulaSettingValue.Value, time.Hour*24)
			} else {
				self.Icon = "/zip/icon/" + self.Name
			}
		} else {
			self.Icon = "data:image/jpg;base64," + iconSetting.FormulaSettingValue.Value
		}
	}
}
