package logic

type Manifest struct {
	Application Application `yaml:"application"`
	Platform    Platform    `yaml:"platform"`
	Bindings    []Bindings  `yaml:"bindings"`
	Domain      string      `yaml:"domain"`
	Source      Source      `yaml:"source"`
	Web         Source      `yaml:"web"`
}

type Application struct {
	Author      string `yaml:"author"`
	Theme       string `yaml:"theme"`
	Name        string `yaml:"name"`
	Identifie   string `yaml:"identifie"`
	Description string `yaml:"description"`
}

type Platform struct {
	Container Container `yaml:"container"`
	Supports  []string  `yaml:"supports"`
	Ingress   []Ingress `yaml:"ingress"`
	Depends   []Depend  `yaml:"depends"`
}

type Container struct {
	ContainerPort       int           `yaml:"containerPort"`
	MinNum              int           `yaml:"minNum"`
	MaxNum              int           `yaml:"maxNum"`
	Cpu                 int           `yaml:"cpu"`
	Mem                 int           `yaml:"mem"`
	PolicyType          string        `yaml:"policyType"`
	PolicyThreshold     int           `yaml:"policyThreshold"`
	CustomLogs          string        `yaml:"customLogs"`
	InitialDelaySeconds int           `yaml:"initialDelaySeconds"`
	Image               string        `yaml:"image"`
	Volumes             []Volumes     `yaml:"volumes"`
	StartParams         []StartParams `yaml:"startParams"`
	Env                 []Env         `yaml:"env"`
	Ports               []Port        `yaml:"ports"`
	Build               Build         `yaml:"build"`
	Privileged          string        `yaml:"privileged"`
	Cmd                 []string      `yaml:"cmd"`
	Hook                struct {
		RequireInstall string `yaml:"requireInstall"`
	} `yaml:"hook"`
	SecurityContext struct {
		FsGroup      int  `yaml:"fsGroup"`
		RunAsGroup   int  `yaml:"runAsGroup"`
		RunAsNonRoot bool `yaml:"runAsNonRoot"`
		RunAsUser    int  `yaml:"runAsUser"`
	} `yaml:"securityContext"`
}

type Volumes struct {
	Type      string `yaml:"type"`
	MountPath string `yaml:"mountPath"`
	SubPath   string `yaml:"subPath"`
}

type StartParams struct {
	Name        string `yaml:"name"`
	Title       string `yaml:"title"`
	Required    bool   `yaml:"required"`
	Type        string `yaml:"type"`
	ValuesText  string `yaml:"values_text"`
	ModuleName  string `yaml:"module_name"`
	Description string `yaml:"description"`
}

type Env struct {
	Name  string `yaml:"name"`
	Value string `yaml:"value"`
}

type Port struct {
	Name string `yaml:"name"`
	Port int    `yaml:"port"`
}

type Build struct {
	Context string
}

type Bindings struct {
	Title             string `yaml:"title"`
	Name              string `yaml:"name"`
	Status            int    `yaml:"status"`
	Framework         string `yaml:"framework"`
	IsDefaultRegister int    `yaml:"is_default_register"`
	Location          string `yaml:"location"`
	Menu              []Menu `yaml:"menu"`
}

type Menu struct {
	Displayorder int    `yaml:"displayorder"`
	Do           string `yaml:"do"`
	Title        string `yaml:"title"`
	Icon         string `yaml:"icon"`
	Location     string `yaml:"location"`
	IsDefault    int    `yaml:"is_default"`
}

type Ingress struct {
	Name   string `yaml:"name"`
	Routes []struct {
		Backend struct {
			Name string `yaml:"name"`
			Port string `yaml:"port"`
		} `yaml:"backend"`
		Path string `yaml:"path"`
	} `yaml:"routes"`
}

type Depend struct {
	Identifie string `yaml:"identifie"`
	Name      string `yaml:"name"`
	From      string `yaml:"from"`
	Required  bool   `yaml:"required"`
}

type Source struct {
	Url  string `yaml:"url"`
	Type string `yaml:"type"`
}
