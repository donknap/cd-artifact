package logic

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
	"time"
)

type LoginUserToken struct {
	OpenId   string `json:"open_id"`
	Nickname string `json:"nickname"`
	jwt.RegisteredClaims
}

type OauthLogic struct {
}

func (self OauthLogic) GetUserJwtToken(userInfo *LoginUserToken) string {
	jwtSecret := []byte(facade.GetConfig().GetString("setting.app_secret"))
	userInfo.RegisteredClaims = jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(12 * time.Hour)),
	}
	jwt := jwt.NewWithClaims(jwt.SigningMethodHS256, userInfo)
	code, err := jwt.SignedString(jwtSecret)
	if err != nil {
		println(err.Error())
	}
	return code
}

func (self OauthLogic) ParseUserJwtToken(jwtToken string) (*LoginUserToken, error) {
	jwtSecret := []byte(facade.GetConfig().GetString("setting.app_secret"))
	myUserInfo := &LoginUserToken{}
	token, err := jwt.ParseWithClaims(jwtToken, myUserInfo, func(t *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	}, jwt.WithValidMethods([]string{"HS256"}))

	if err != nil {
		return nil, err
	}
	if token.Valid {
		return myUserInfo, nil
	} else {
		return nil, errors.New("解析失败")
	}
}
