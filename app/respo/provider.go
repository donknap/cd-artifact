package respo

import (
	"embed"

	"gitee.com/donknap/cd-artifact/app/respo/command"
	"gitee.com/donknap/cd-artifact/app/respo/http/controller"
	"gitee.com/donknap/cd-artifact/app/respo/http/middleware"
	"gitee.com/donknap/cd-artifact/app/respo/logic"
	"github.com/gin-gonic/gin"
	"github.com/we7coreteam/w7-rangine-go-support/src/console"
	http_server "github.com/we7coreteam/w7-rangine-go/src/http/server"
)

type Provider struct {
}

func (provider *Provider) Register(httpServer *http_server.Server, console console.Console, asset embed.FS) {
	// 注册一个 respo:test 命令
	console.RegisterCommand(new(command.Test))
	// 初始化本地仓库
	err := logic.RegisterDepot()
	if err != nil {
		panic(err)
	}

	// 注册一些路由
	httpServer.RegisterRouters(func(engine *gin.Engine) {
		engine.GET("/", middleware.Home{}.Process, controller.Home{}.Index)

		cors := engine.Group("/", middleware.Cors{}.Process)

		group := cors.Group("/respo")
		group.Match([]string{"POST", "OPTIONS"}, "/add", middleware.CheckUserLogin{}.Process, controller.Home{}.Add)
		group.Match([]string{"POST", "OPTIONS"}, "/delete", middleware.CheckUserLogin{}.Process, controller.Home{}.Delete)
		group.Match([]string{"POST", "OPTIONS"}, "/file", middleware.CheckUserLogin{}.Process, controller.File{}.File)
		group.Match([]string{"POST", "OPTIONS"}, "/path-tree", middleware.CheckUserLogin{}.Process, controller.File{}.PathTree)
		group.Match([]string{"POST", "OPTIONS"}, "/publish", middleware.CheckUserLogin{}.Process, controller.Home{}.Publish)
		group.Match([]string{"GET", "OPTIONS"}, "/list", controller.Home{}.List)
		group.Match([]string{"GET", "OPTIONS"}, "/detail/:id", controller.Home{}.Detail)
		group.Match([]string{"GET", "OPTIONS"}, "/info/:id", controller.Home{}.Info)
		group.Match([]string{"GET", "OPTIONS"}, "/info/:id/:cid", controller.Home{}.Info)
		group.Match([]string{"POST", "OPTIONS"}, "/icon", middleware.CheckUserLogin{}.Process, controller.Home{}.Icon)
		group.Match([]string{"POST", "OPTIONS"}, "/status", middleware.CheckUserLogin{}.Process, controller.Home{}.Status)
		// 版本相关
		group.Match([]string{"POST", "OPTIONS"}, "/version-list", middleware.CheckUserLogin{}.Process, controller.Version{}.GetList)
		group.Match([]string{"POST", "OPTIONS"}, "/version-add", middleware.CheckUserLogin{}.Process, controller.Version{}.Add)
		group.Match([]string{"POST", "OPTIONS"}, "/version-publish", middleware.CheckUserLogin{}.Process, controller.Version{}.Publish)
		// 制品压缩包管理
		group.Match([]string{"POST", "OPTIONS"}, "/get-zip-file-list", middleware.CheckUserLogin{}.Process, controller.File{}.GetZipFileList)
		group.Match([]string{"POST", "OPTIONS"}, "/get-zip-file-content", middleware.CheckUserLogin{}.Process, controller.File{}.GetZipFileContent)
		group.Match([]string{"POST", "OPTIONS"}, "/delete-zip-file", middleware.CheckUserLogin{}.Process, controller.File{}.GetZipFileContent)

		cors.Match([]string{"POST", "OPTIONS"}, "/zip/upload", middleware.CheckUserLogin{}.Process, controller.Zip{}.Upload)
		engine.GET("/zip/download/:id", controller.Zip{}.Download)
		engine.GET("/zip/icon/*path", controller.Zip{}.Icon)

		cors.Match([]string{"POST", "OPTIONS"}, "/oauth/login", controller.Oauth{}.Login)

		// 标签
		cors.Match([]string{"POST", "OPTIONS"}, "/respo/tag/add", controller.Tag{}.Add)
		cors.Match([]string{"POST", "OPTIONS"}, "/respo/tag/delete", controller.Tag{}.Delete)
		cors.Match([]string{"POST", "OPTIONS"}, "/respo/tag/list", controller.Tag{}.List)

		cors.Match([]string{"GET", "OPTIONS"}, "/static/*path", controller.Static{}.File)
	})
	depot, _ := logic.NewDepot()
	depot.InitBaseDepot()
	go depot.SyncZip()
}
