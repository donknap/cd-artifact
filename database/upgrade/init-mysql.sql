-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2023-09-08 02:59:13
-- 服务器版本： 8.0.28
-- PHP 版本： 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- 数据库： `w7-w7_cd_artifact`
--

-- --------------------------------------------------------

--
-- 表的结构 `ims_formula`
--

CREATE TABLE `ims_formula` (
                               `id` int NOT NULL,
                               `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标识',
                               `title` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                               `version_latest_id` int NOT NULL COMMENT '版本序号',
                               `created_at` int NOT NULL,
                               `updated_at` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ims_formula_setting`
--

CREATE TABLE `ims_formula_setting` (
                                       `id` int NOT NULL,
                                       `formula_id` int NOT NULL,
                                       `version_id` int NOT NULL,
                                       `setting_value_id` int NOT NULL,
                                       `type` tinyint NOT NULL DEFAULT '1' COMMENT '1为文件，2为配置',
                                       `name` varchar(20) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ims_formula_setting_value`
--

CREATE TABLE `ims_formula_setting_value` (
                                             `id` int NOT NULL,
                                             `value` longtext COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ims_version`
--

CREATE TABLE `ims_version` (
                               `id` int NOT NULL,
                               `formula_id` int NOT NULL,
                               `name` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
                               `description` varchar(100) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `ims_formula`
--
ALTER TABLE `ims_formula`
    ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- 表的索引 `ims_formula_setting`
--
ALTER TABLE `ims_formula_setting`
    ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ims_formula_setting_value`
--
ALTER TABLE `ims_formula_setting_value`
    ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ims_version`
--
ALTER TABLE `ims_version`
    ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `ims_formula`
--
ALTER TABLE `ims_formula`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ims_formula_setting`
--
ALTER TABLE `ims_formula_setting`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ims_formula_setting_value`
--
ALTER TABLE `ims_formula_setting_value`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ims_version`
--
ALTER TABLE `ims_version`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;
