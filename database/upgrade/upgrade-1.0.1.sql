-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2023-09-21 09:12:25
-- 服务器版本： 8.0.28
-- PHP 版本： 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

ALTER TABLE `ims_formula` ADD `install_total` INT NULL DEFAULT '0' AFTER `version_latest_id`;

COMMIT;
