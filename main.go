package main

import (
	"embed"
	_ "embed"

	"gitee.com/donknap/cd-artifact/app/attach"
	"gitee.com/donknap/cd-artifact/app/respo"
	"gitee.com/donknap/cd-artifact/common/dao"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"github.com/we7coreteam/w7-rangine-go-support/src/facade"
	app "github.com/we7coreteam/w7-rangine-go/src"
	"github.com/we7coreteam/w7-rangine-go/src/core/err_handler"
	"github.com/we7coreteam/w7-rangine-go/src/http"
	"github.com/we7coreteam/w7-rangine-go/src/http/middleware"
	"github.com/we7coreteam/w7-rangine-go/src/http/response"
)

//go:embed config.yaml
var ConfigFile []byte

//go:embed asset/*
var asset embed.FS

type provider struct {
	ConfigDefault func(viper *viper.Viper)
	EventDault    func()
}

func main() {
	app := app.NewApp(app.Option{
		Name: "w7-rangine-go-skeleton",
	})
	// 业务中需要使用 http server，这里需要先实例化111
	httpServer := new(http.Provider).Register(app.GetConfig(), app.GetConsole(), app.GetServerManager()).Export()
	// 注册一些全局中间件，路由或是其它一些全局操作
	httpServer.Use(middleware.GetPanicHandlerMiddleware())
	httpServer.RegisterRouters(func(engine *gin.Engine) {
		// cors := engine.Group("/", middleware2.Cors{}.Process)
		// cors.Static("/static", facade.GetConfig().GetString("setting.storage.local.path")+"/Static")
		engine.NoRoute(func(context *gin.Context) {
			context.String(404, "404 Not Found")
		})
	})

	response.SetErrResponseHandler(func(ctx *gin.Context, env string, err error, statusCode int) {
		ctx.JSON(statusCode, map[string]interface{}{
			"code":  statusCode,
			"error": err.Error(),
		})
	})
	db, err := facade.GetDbFactory().Channel("default")
	if err_handler.Found(err) {
		panic(err)
	}
	dao.SetDefault(db)

	// 注册业务 provider，此模块中需要使用 http server 和 console
	new(respo.Provider).Register(httpServer, app.GetConsole(), asset)
	new(attach.Provider).Register(httpServer, app.GetConsole())
	app.RunConsole()
}
