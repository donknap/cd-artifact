#FROM golang:latest as builder
#WORKDIR /home
#COPY . .
#RUN export CGO_ENABLED=0 && export GOARCH=amd64 && export GOOS=linux && go build -o  /home/server

FROM alpine:3.18
WORKDIR /home

ENV DEPOT_LOCAL_BASE_DIR /var/artifact
ENV SERVER_PORT 8081
ENV MYSQL_HOST ""
ENV MYSQL_PASSWORD ""
ENV MYSQL_USERNAME ""
ENV MYSQL_PORT 3306
ENV MYSQL_DATABASE "w7-cd-artifact"
ENV DEPOT_COS_SECRET_ID ""
ENV DEPOT_COS_SECRET_KEY ""
ENV DEPOT_COS_REGION ""
ENV DEPOT_COS_ENDPOINT ""
ENV DEPOT_COS_BUCKET ""
ENV DOMAIN_URL ""
ENV API_URL ""


#COPY --from=builder /home/server /home
ADD ./docker/server /home/server
RUN echo "https://mirrors.cloud.tencent.com/alpine/v3.18/main" > /etc/apk/repositories && \
    echo "https://mirrors.cloud.tencent.com/alpine/v3.18/community" >> /etc/apk/repositories
RUN apk --no-cache add zip mysql-client
ADD ./docker/start.sh /home/start.sh
ADD ./config.yaml /home/config.yaml
ADD ./database/upgrade /home/upgrade
RUN mkdir -p /var/artifact/Storage && mkdir -p /var/artifact/Formula && chmod -R 764 /var/artifact
RUN touch /var/artifact/.keep1

VOLUME ["/var/artifact"]
EXPOSE 8081

ENTRYPOINT ["sh", "/home/start.sh"]
#CMD tail -f